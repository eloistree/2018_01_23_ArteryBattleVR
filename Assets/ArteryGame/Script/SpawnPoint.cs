﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public interface InterfaceSpawnPoint {

    Vector3 GetPosition();
    Quaternion GetRotation();
    bool IsUsed();
}
public class SpawnPoint : MonoBehaviour, InterfaceSpawnPoint {

    public bool m_used = false;
    public Vector3 Position { get { return transform.position; } }
    public Quaternion Rotation { get { return transform.rotation; } }


    public void Clamp()
    {
        m_used = true;
    }
    public void Unclamp()
    {
        m_used = false;
    }
    public static void SwitchSpot(SpawnPoint previousSpot, out SpawnPoint newSpot, bool mustNotBeTheSame)
    {
        SpawnPoint spawnPoint = GetRandomPoint(true, mustNotBeTheSame?previousSpot: null);
        spawnPoint.Clamp();

        if (previousSpot)
            previousSpot.Unclamp();

        newSpot = spawnPoint; ;
    }

    public static void Unclamp(SpawnPoint point) {
        point.m_used = false;
    }
    public static SpawnPoint GetRandomPoint ( bool onlyNotUsed, SpawnPoint previous=null) {
        SpawnPoint result = null ;
        SpawnPoint []  spawns =  FindObjectsOfType<SpawnPoint>();
        if (spawns.Length == 0)
            return null;
        int antiLoop = 100;
        while (result == null || antiLoop > 0) {
            antiLoop--;
            result = spawns[UnityEngine.Random.Range(0, spawns.Length)];
            if (result.IsUsed() || result == previous)
                result = null;
        }

        if (result == null) {
            Debug.Log("No spawn found");
            result = spawns[UnityEngine.Random.Range(0, spawns.Length)];
        }

        return result;

	}
    
    
    internal static void Reposition(Transform affected, SpawnPoint where)
    {
        if(where != null)
            affected.position = where.Position;
    }

    public Vector3 GetPosition()
    {
        return Position;
    }

    public Quaternion GetRotation()
    {
        return Rotation;
    }

    public bool IsUsed()
    {
        return m_used;
    }
}
