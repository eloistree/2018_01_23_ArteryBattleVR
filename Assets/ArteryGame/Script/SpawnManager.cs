﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    public Transform _affectedPosition;
    public bool m_usedRandomSpawnAtStart =true;
    public SpawnPoint m_usedSpawn;

    void Awake () {
        if (m_usedRandomSpawnAtStart)
        {
            GetNewPosition();
        }
	}
	
	void Reset () {
        _affectedPosition = transform;
	}

    public void GetNewPosition() {
        SpawnPoint newPoint;
        SpawnPoint.SwitchSpot(m_usedSpawn, out newPoint, true);
        if (newPoint != null) {
            _affectedPosition.position = newPoint.Position;
            _affectedPosition.rotation = newPoint.Rotation;
        }

        m_usedSpawn = newPoint;


    }
}
